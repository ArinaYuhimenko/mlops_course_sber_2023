import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import math
import os

for dirname, _, filenames in os.walk('/kaggle/input'):
    for filename in filenames:
        print(os.path.join(dirname, filename))

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
from sklearn.model_selection import train_test_split

train = pd.read_csv('/kaggle/input/hw-multiclass-classification/train.csv', sep=';')
test = pd.read_csv('/kaggle/input/hw-multiclass-classification/test.csv', sep=';')
submission = pd.read_csv('/kaggle/input/hw-multiclass-classification/sub_baseline.csv')

'train: ', train.shape, 'test: ', test.shape  # оцениваем размер

submission.head()


train.columns

train.head()

train.interest_level.value_counts()

train.interest_level.value_counts(normalize=True)

# **Строки превращаем обратно в список**

train.features

train['features'] = train['features'].str.replace('[\[\]\']', '').str.split(', ')

train['features']

train['num_features'] = train['features'].apply(len)


train['photos'] = train['photos'].str.replace('[\[\]\']', '').str.split(', ')
train['num_photos'] = train['photos'].apply(len)

# В датасете присутствуют NaN, поэтому предварительно заполняем пропуски пустыми строками.

train["num_description_words"] = train["description"].fillna('').apply(lambda x: len(x.split(" ")))


train["created"]

train["created"] = pd.to_datetime(train["created"])
train["created_year"] = train["created"].dt.year
train["created_month"] = train["created"].dt.month
train["created_day"] = train["created"].dt.day
train["created_hour"] = train["created"].dt.hour

hours = train["created_hour"].value_counts().reset_index().sort_values(by='index')
hours.plot(x='index', y='created_hour', kind='bar')

months = train["created_month"].value_counts().reset_index().sort_values(by='index')
months.plot(x='index', y='created_month', kind='bar')

train.head()

train.num_description_words.value_counts()

import matplotlib.pyplot as plt
import seaborn as sns
% matplotlib
inline
sns.countplot(x='interest_level', data=train)

# Видим, что в большинстве случаев спрос низкий

sns.catplot(x='interest_level', y='num_description_words', kind='violin', data=train);

# По этому графику можно отметить, что у низкого спроса нижний квартиль ниже, чем высокого и среднего. Но и разброс очень сильный. У среднего верхний квартиль выше остальных.
train.bedrooms.value_counts()
sns.countplot(x='interest_level', hue='bedrooms', data=train)
# Видим, что однокомнатные квартиры в большинстве не пользуются спросом
train.price.value_counts()
train['price'].mean()
train.num_photos.value_counts()
sns.catplot(x='interest_level', y='num_photos', kind='bar', data=train);
# В Seaborn функция barplot() по умолчанию отображает среднее значение величины внутри каждой категории и отображает ее доверительный интервал. Видим, что у высокого и среднего спроса количество фотографий больше.
train.head()
train.num_features.value_counts()
sns.catplot(x='interest_level', y='num_features', kind='bar', data=train);
# По графику видим, что доверительный интервал у высого спроса больше остальных
sns.catplot(x='interest_level',
            y='bedrooms',
            hue='bathrooms',
            data=train);

target = 'interest_level'
features = ['num_features', 'num_photos', 'price', 'num_description_words', 'bedrooms', 'bathrooms', 'latitude',
            'longitude']
from sklearn.preprocessing import OneHotEncoder

oe_style = OneHotEncoder()
oe_results = oe_style.fit_transform(train[["interest_level"]])
oe_results.toarray()
train.head()

mapper = {
    'low': 0,
    'medium': 1,
    'high': 2
}

train['interest_level'] = train['interest_level'].apply(lambda x: mapper[x])

train.head()
# Т.к. в столбце interest_level находятся не числа, а строки, когда остальные отобранные нами признаки являются числами.
train[features].head()
from sklearn.model_selection import train_test_split, StratifiedKFold, RandomizedSearchCV
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from xgboost import XGBClassifier
from sklearn.metrics import f1_score, average_precision_score
import joblib
train[features].head()
classifiers = {
    'KNeighborsClassifier': KNeighborsClassifier(),
    'DecisionTreeClassifier': DecisionTreeClassifier(),
    'RandomForestClassifier': RandomForestClassifier(),
    'MLPClassifier': MLPClassifier(),
    'AdaBoostClassifier': AdaBoostClassifier(),
    'GaussianNB': GaussianNB(),
    'QuadraticDiscriminantAnalysis': QuadraticDiscriminantAnalysis(),
    'XGBClassifier': XGBClassifier(),

}
from sklearn.preprocessing import StandardScaler, Normalizer
scaler = StandardScaler()
scaled_train = scaler.fit_transform(train[features])

len(features)

pd.DataFrame(scaled_train).describe()

# Делим выборку на train и test,
X_train, X_test, y_train, y_test = train_test_split(scaled_train, train[target],
                                                    test_size=0.3, random_state=0)

X_train.shape, X_test.shape, y_train.shape, y_test.shape

for name, model in classifiers.items():
    print(name)

    model.fit(X_train, y_train)
    predicts = model.predict(X_test)
    f1 = f1_score(y_test, predicts, average='macro')

    print('На отложенной тестовой выборке: ')
    print('%s: F_мера: %7.5f ' %
          (name, f1))
    print('-' * 30)

model = RandomForestClassifier(n_estimators=100, max_depth=None, random_state=0)
model.fit(X_train, y_train)  # обучаем модель
prediction = model.predict(X_test)  # делаем предсказание

from sklearn.metrics import accuracy_score, f1_score, average_precision_score, roc_auc_score, classification_report, \
    precision_score, recall_score

print(classification_report(y_test, prediction))

logistic_regression = LogisticRegression()

logistic_regression.fit(X_train, y_train)
prediction = logistic_regression.predict(X_test)

print(classification_report(y_test, prediction))
xgb = XGBClassifier(n_estimators=100, seed=42)
xgb.fit(X_train, y_train)
predicts = xgb.predict(X_test)

print(classification_report(y_test, predicts))

from sklearn.svm import LinearSVC

scv = LinearSVC(random_state=0)
scv.fit(X_train, y_train)
predicts = scv.predict(X_test)

print(classification_report(y_test, predicts))

from sklearn.neighbors import KNeighborsClassifier

kNN = KNeighborsClassifier(n_neighbors=20)
kNN.fit(X_train, y_train)
predicts = kNN.predict(X_test)

print(classification_report(y_test, predicts))


model = DecisionTreeClassifier(random_state=0, class_weight='balanced')
model.fit(X_train, y_train)
predicts = model.predict(X_test)

print(classification_report(y_test, predicts))

from sklearn.pipeline import Pipeline
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.feature_selection import RFE
import numpy as np
from sklearn.ensemble import GradientBoostingClassifier

from sklearn.feature_selection import RFECVrfecv = RFECV(estimator=GradientBoostingClassifier())


from sklearn.ensemble import RandomForestClassifier

# создаем случайное дерево с гипер параметрами
model = RandomForestClassifier(n_estimators=340)
model.fit(X_train, y_train)

# Подбираем самые важные признаки
importances = model.feature_importances_

# Создаем отдельный датасет для визуализации
final_df = pd.DataFrame({"features": pd.DataFrame(X_train).columns, "Importances": importances})
final_df.set_index('Importances')

# Сортируем их по возрастанию для лучшей визуализации
final_df = final_df.sort_values('Importances')

# Выводим на график
final_df.plot.bar(color='teal')

# Абсолютное отклонение (Mean Absolute Difference, MAD)
# Вычисляем MAD
# Где X - входные данные
mean_absolute_difference = np.sum(np.abs(X_train - np.mean(X_train, axis=0)), axis=0) / X_train.shape[0]

# график признаков
plt.bar(np.arange(X_train.shape[1]), mean_absolute_difference, color='teal')

from sklearn.model_selection import GridSearchCV

from sklearn.model_selection import train_test_split, StratifiedKFold, RandomizedSearchCV

skf = StratifiedKFold(n_splits=3, shuffle=True, random_state=0)
params = {
    'max_depth': [2, 3, 4, 5],
    "min_samples_leaf": list(range(1, 5))
}

rf = RandomForestClassifier(n_estimators=100, random_state=0,
                            n_jobs=-1, oob_score=True)

dt_random_search = RandomizedSearchCV(rf, params, n_jobs=-1, cv=skf, verbose=1)
dt_random_search.fit(X_train, y_train)

print(dt_random_search.best_estimator_)
print(dt_random_search.best_params_)

from sklearn.ensemble import VotingClassifier

clf1 = LogisticRegression(random_state=1)
clf2 = RandomForestClassifier(n_estimators=50, random_state=1)
clf3 = XGBClassifier()

eclf = VotingClassifier(
    estimators=[('lr', clf1), ('rf', clf2), ('gnb', clf3)],
    voting='hard')
eclf.fit(X_train, y_train)
predicts = eclf.predict(X_test)
f1 = f1_score(y_test, predicts, average='macro')
print('На отложенной тестовой выборке: ')
print('%s: F_мера: %7.5f, ' %
      ('Ensemble', f1))

from sklearn import datasets
from sklearn.cluster import KMeans

# Описываем модель
model = KMeans(n_clusters=3)

# Проводим моделирование
model.fit(X_train, y_train)

# Предсказание на всем наборе данных
all_predictions = model.predict(X_test)

# Выводим предсказания
print(all_predictions)

features = ['num_features', 'num_photos', 'price', 'num_description_words', 'bedrooms', 'bathrooms', 'latitude',
            'longitude']
target = 'interest_level'

train = train.set_index('listing_id')
train_target = train[target]

X_train, X_test, y_train, y_test = train_test_split(train[features], train_target,
                                                    test_size=0.3, stratify=train_target,
                                                    random_state=0)

y_train.value_counts(normalize=True)

y_test.value_counts(normalize=True)

model = XGBClassifier()
model.fit(train[features], train[target])

test['features'] = test['features'].str.replace('[\[\]\']', '').str.split(', ')
test['num_features'] = test['features'].apply(len)
test['photos'] = test['photos'].str.replace('[\[\]\']', '').str.split(', ')
test['num_photos'] = test['photos'].apply(len)
test["num_description_words"] = test["description"].fillna('').apply(lambda x: len(x.split(" ")))
test["created"] = pd.to_datetime(test["created"])
test["created_year"] = test["created"].dt.year
test["created_month"] = test["created"].dt.month
test["created_day"] = test["created"].dt.day
test["created_hour"] = test["created"].dt.hour

test[target] = model.predict(test[features])

test[['listing_id', target]].to_csv('sub_baseline.csv', index=None)

